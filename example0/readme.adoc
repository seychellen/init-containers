= Example0

 ▶ oc new-project init-containers
 ▶ oc new-app twalter/openshift-nginx:stable --name nginx-stable
 ▶ oc expose svc nginx-stable --port=8081
 route.route.openshift.io/nginx-stable exposed
 ▶ oc delete all --all -n init-containers

Im Browser mit http://route.route.openshift.io/nginx-stable aufrufen.