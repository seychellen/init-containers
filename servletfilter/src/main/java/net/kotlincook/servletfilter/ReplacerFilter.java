package net.kotlincook.servletfilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ReplacerFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {

        var contextPath = System.getenv("CONTEXT_PATH");
        if (contextPath == null) contextPath = "servletfilter";

        var placeholderDefaultUrl = System.getenv("PLACEHOLDER_DEFAULT_URL");
        if (placeholderDefaultUrl == null) placeholderDefaultUrl = "http://default.com";


        var wrapper = new CharResponseWrapper((HttpServletResponse) response);
        filterChain.doFilter(request, wrapper);

        var originalContent = wrapper.toString();

        ServletOutputStream out = response.getOutputStream();
        var alteredContent = originalContent
                .replace("CONTEXT_PATH", contextPath)
                .replace("PLACEHOLDER_DEFAULT_URL", placeholderDefaultUrl);

        response.setContentLength(alteredContent.length());
        out.write(alteredContent.getBytes(StandardCharsets.UTF_8));
    }

}
