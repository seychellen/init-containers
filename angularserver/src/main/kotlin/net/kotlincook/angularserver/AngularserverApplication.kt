package net.kotlincook.angularserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer

@SpringBootApplication
class AngularserverApplication: SpringBootServletInitializer()

fun main(args: Array<String>) {
	runApplication<AngularserverApplication>(*args)
}
