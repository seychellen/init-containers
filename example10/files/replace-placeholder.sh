#!/bin/sh

WAR_NAME=$(ls *.war)
EXTRACT_DIR=extract
echo "unzipping $WAR_NAME..."
unzip -d $EXTRACT_DIR $WAR_NAME
echo "...done, replacing placeholders..."
sed -i.backup -e "s/CONTEXT_PATH/$CONTEXT_PATH/g" $EXTRACT_DIR/index.html
sed -i.backup -e "s/PLACEHOLDER_DEFAULT_URL/$PLACEHOLDER_DEFAULT_URL/g" $EXTRACT_DIR/*.js
echo "...done, removing backup..."
rm $EXTRACT_DIR/*.backup
echo "...done, zipping modified files..."
cd $EXTRACT_DIR
zip -r "../$EXTRACT_DIR.temp" *
cd ..
mv $EXTRACT_DIR.temp $WAR_NAME
echo "...done, deleting directory $EXTRACT_DIR"
rm -r $EXTRACT_DIR
echo "...done"