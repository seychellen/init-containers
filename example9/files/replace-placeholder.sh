#!/bin/sh

WAR_NAME=$(ls *.war)
EXTRACT_DIR=extract
echo "unzipping $WAR_NAME..."
unzip -d $EXTRACT_DIR *.war
echo "...done, replacing placeholders..."
sed -i.backup -e 's/Hello World/Hallo Welt/g' $EXTRACT_DIR/*.jsp
echo "...done, removing backup..."
rm $EXTRACT_DIR/*.backup
echo "...done, zipping modified files..."
zip -jr "$EXTRACT_DIR.zip" $EXTRACT_DIR
mv "$EXTRACT_DIR.zip" $WAR_NAME
echo "...done, deleting directory $EXTRACT_DIR"
rm -r $EXTRACT_DIR
echo "...done"