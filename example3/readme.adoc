= Example3

== Mit Service und Route

 oc create --save-config -f deployment.yaml

Um einen Service "on the fly" zu erstellen:

 ▶ oc expose deployment wildfly-deploy --type NodePort --port 8080
 service/wildfly-deploy exposed

Um eine Route "on the fly" zu erstellen:

 ▶ oc expose svc wildfly-deploy --port=8080
 route.route.openshift.io/wildfly-deploy exposed

Zugriff mit http://wildfly-deploy-init-containers.apps-crc.testing/

Alles wieder beseitigen:

 ▶ oc delete all --all -n init-containers

== Mit Port-Forward ausprobieren

 ▶ oc create --save-config -f deployment.yaml

 ▶ oc get pods
 NAME                             READY   STATUS    RESTARTS   AGE
 wildfly-deploy-f5ccf7695-zjz5f   1/1     Running   0          2m43s

 ▶ oc port-forward pod/wildfly-deploy-f5ccf7695-zjz5f 8080:8080

Zugriff mit http://localhost:8080/helloworld.

